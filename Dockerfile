FROM node:14-alpine
WORKDIR /usr/src/app
COPY package*.json ./
RUN ["npm", "ci"]
COPY . .
EXPOSE 3000

CMD ["sh","-c","npm install --save react-bootstrap-validation ; npm start"]