// import profile_picture from "../assets/icons/default_profile_picture.jpg"
import add_entry from "../assets/icons/add_icon.svg"
import prev_page from "../assets/icons/prev_page_icon.png"
import next_page from "../assets/icons/next_page_icon.png"
import Message from './Message';
import React, { useState } from "react";
import PostMessageModal from "../modals/PostMessage";

const page_size = 3;

const MessagePanel = ( {messages} ) => {
    const [start, setStart] = useState(0);
    const [displayModal, setDisplayModal] = useState(false);
    
    const prev_page_f = () => {
        setStart(Math.max(0, start - page_size));
    };
    
    const next_page_f = () => {
        let tmp = Math.min(messages.length - page_size, start + page_size);
        if (tmp < page_size) {
            tmp = 0;
        }
        setStart(tmp);
    };

    const showModal = () => {
        setDisplayModal(true);
      };
    
    const hideModal = () => {
        setDisplayModal(false);
    };

    return (
        <div>
            <PostMessageModal show={displayModal} handleClose={hideModal}></PostMessageModal>
            <div className="container">
                {
                messages.slice(start, start + page_size).map(({ idNum, fullname, resource, location, quantity, deadline, description }, index) => (
                    <Message key={index} idNum={idNum} fullname={fullname} resource={resource} location={location} quantity={quantity} deadline={deadline} description={description}/>
                ))}
            </div>
            <div className="container">
                <div className="row">
                    <button className="btn col-sm d-flex justify-content-center" onClick={prev_page_f}><img className="settings-icon" src={prev_page} alt=''/></button>
                    <button className="btn col-sm d-flex justify-content-center" onClick={next_page_f}><img className="settings-icon" src={next_page} alt=''/></button>
                    <button className="btn col-sm d-flex justify-content-center" onClick={showModal}><img className="settings-icon" src={add_entry} alt=''/></button>
                </div>
            </div>
        </div>
    )
}

export default MessagePanel;
