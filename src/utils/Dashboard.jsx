import React from "react";
import { Link } from "react-router-dom";
import { useLocation } from 'react-router-dom';
import { Button } from "react-bootstrap";
import { useNavigate } from "react-router";
import { useUserAuth } from "../context/UserAuthContext";

function HeaderView() {
    const location = useLocation();
    console.log(location.pathname);
    return location.pathname;
};

const genericRoute = (name) => {
    const to_path = '/' + name;
    var cls = "nav-link scrollto";
    if (HeaderView() === to_path) {
        cls += " active";
    }
    const text = name.charAt(0).toUpperCase() + name.slice(1);
    return <Link to={to_path}><div className={cls}>{text}</div></Link>;
};

const Dashboard = ( ) => {
    const { logOut, user } = useUserAuth();
    const navigate = useNavigate();
    const handleLogout = async () => {
        try {
        await logOut();
        navigate("/");
        } catch (error) {
        console.log(error.message);
        }
    };

    return (
        <div id="header" className="d-flex align-items-center">
            <div className="container d-flex align-items-center">
                <h1 className="logo me-auto">
                    <Link to='/home'>Web-Supply</Link>
                </h1>

                <nav id="navbar" className="navbar">
                    <ul>
                        <li className="mx-3">{genericRoute('home')}</li>
                        <li className="mx-3">{genericRoute('profile')}</li>
                        <li className="mx-3">{genericRoute('stats')}</li>
                        <li className="mx-3"><Button variant="outline-success" onClick={handleLogout}>Logout</Button></li>
                    </ul>
                </nav>
            </div>
        </div>
    )
}

export default Dashboard;
