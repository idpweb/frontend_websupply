import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import UserHome from "../pages/User/UserHome"
import Login from "../pages/General/Login";
import Register from "../pages/General/Register";
import UserProfile from "../pages/User/UserProfile";
import UserStats from "../pages/User/UserStats";
import ProtectedRoute from "../pages/General/ProtectedRoute";
import { UserAuthContextProvider } from "../context/UserAuthContext";

const Router = () => {
    return (
        <BrowserRouter>
            <UserAuthContextProvider>
                <Routes>
                    <Route
                        path="/home"
                        element={
                        <ProtectedRoute>
                            <UserHome />
                        </ProtectedRoute>
                        }
                    />
                    <Route
                        path="/profile"
                        element={
                        <ProtectedRoute>
                            <UserProfile />
                        </ProtectedRoute>
                        }
                    />
                    <Route
                        path="/stats"
                        element={
                        <ProtectedRoute>
                            <UserStats />
                        </ProtectedRoute>
                        }
                    />
                    <Route path="/" element={<Login />} />
                    <Route path="/register" element={<Register />} />
                </Routes>
            </UserAuthContextProvider>
        </BrowserRouter>
    );
}

export default Router;
