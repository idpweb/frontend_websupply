import { Button, Col, Row, Stack } from "react-bootstrap";
import React, { useState } from "react";
import EditMessage from '../modals/EditMessage'
import { createImageFromInitials } from "./ProfilePictureUtils";
import ProfileCard from "../modals/ProfileCard";

const Message = (props) => {
    const viewerRole = "owner";
    const imageSize = 330;

    /* ProfileCard Modal */
    const [viewProfileCard, setViewProfileCard] = useState(false);
    const showProfileCard = () => {
        setViewProfileCard(true);
    };
    
    const hideProfileCard = () => {
        setViewProfileCard(false);
    };

    /* Edit Modal */
    const [editModal, setEditModal] = useState(false);
    const showEditModal = () => {
        setEditModal(true);
      };
    
    const hideEditModal = () => {
        setEditModal(false);
    };

    /* Delete function */
    // const DeleteFunc = () => {
    //     /* Implement in backend */
    // }

    const generateButton = (role) => {
        if (role === "user") {
            return;
        }
        var txt = role === "owner" ? "Edit" : "Delete";
        var cls = "interact-btn btn btn-circle ";
        cls += (role === "owner") ? "btn-warning msg-edit" : "btn-danger msg-delete";
        return <Button type="button" className={cls} onClick={(role === "owner") ? showEditModal : () => {}}>{txt}</Button>
    }
    
    return (
        <div className="member-entry"> 
            <ProfileCard show={viewProfileCard} handleClose={hideProfileCard} fullname={props.fullname}></ProfileCard>
            <EditMessage show={editModal} handleClose={hideEditModal} msgProps={props}></EditMessage>
            <div className="member-img"> 
                <img src={createImageFromInitials(imageSize, props.fullname)} className="rounded-circle" onClick={showProfileCard} alt=""/> 
            </div>
                <Row>
                    <Col md="auto"><h2>{props.fullname}</h2></Col>
                    <Col md="auto">#{props.idNum}</Col>
                </Row>
                <Row>
                    <Col md="auto"><button type="button" className="resource btn btn-success">{props.resource}</button></Col>
                    <Col md="auto"><button type="button" className="btn btn-primary">{props.quantity} kg</button></Col>
                    <Col md="auto"><button type="button" className="btn btn-warning">{props.location}</button></Col>
                    <Col md="auto"><button type="button" className="btn btn-danger">{props.deadline}</button></Col>
                    <Col md={{ span: 3, offset: 4 }}>
                        <Stack>
                            <Row><Button type="button" className="interact-btn btn btn-success btn-circle">Interact</Button></Row>
                            <Row>{generateButton(viewerRole)}</Row>
                        </Stack>
                    </Col>
                </Row>
                <Row>
                    <p>{props.description}</p>
                </Row>
            <div className="row">
            </div> 
        </div>
    )
}

export default Message;
