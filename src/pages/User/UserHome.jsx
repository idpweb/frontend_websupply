import React from "react";
import Dashboard from '../../utils/Dashboard';
import MessagePanel from "../../utils/MessagePanel";

const types = ['Offer', 'Request'];
const resources = ["Food", "Water", "Clothing", "Shelter"]
const locations = [
    "Kyiv", "Kharkiv", "Odessa", "Dnipro",
    "Donetsk", "Lviv", "Kryvyi Rih",
    , "Tiachiv", "Semenivka",
    "Bucharest", "Iasi", "Vaslui", "Suceava"];

const random_from_array = (arr) => {
    return arr[Math.floor(Math.random()*arr.length)];
};

const gen_example = (id) => {
    return {
        idNum: id,
        fullname: "Test Client" + id,
        type: random_from_array(types),
        resource: random_from_array(resources),
        quantity: "100",
        location: random_from_array(locations),
        deadline: "26.05.2022",
        description: "Hi"
    };
}

const messages = [gen_example(1), gen_example(2), gen_example(3), gen_example(4), gen_example(5), gen_example(6), gen_example(7)];

const UserHome = () => {
    return (
        <div>
            <Dashboard />
            <MessagePanel messages={messages}/>
        </div>
    )
}

export default UserHome;