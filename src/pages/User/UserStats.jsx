import React from "react";
import Dashboard from '../../utils/Dashboard';
import MessagePanel from "../../utils/MessagePanel";

const gen_example = (id) => {
    return {
        idNum: id,
        fullname: "Owner Client" + id,
        type: 'Offer',
        resource: "Water",
        quantity: "100",
        location: "Bucharest",
        deadline: "19.05.2022",
        description: "Hi I`d like to help.",
    };
}

const messages = [gen_example(1), gen_example(2)];

const UserStats = () => {
    return (
        <div>
            <Dashboard />
            <MessagePanel messages={messages} viewerRole="admin"/>
        </div>
    )
}

export default UserStats;