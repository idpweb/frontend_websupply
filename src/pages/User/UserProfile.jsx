import React, { useState } from "react";
import Dashboard from '../../utils/Dashboard';
import ChangePassword from "../../modals/ChangePassword";
import { createImageFromInitials } from "../../utils/ProfilePictureUtils";

const UserProfile = () => {
    const username = "username_placeholder";
    const email = "email@placeholder.com"
    const fullname = "Test Client";
    const member_since = "19.03.2022";
    const imageSize = 150;

    const [changePass, setChangePass] = useState(false);
    const showChangePass = () => {
        setChangePass(true);
      };
    
    const hideChangePass = () => {
        setChangePass(false);
    };

    return (
        <div>
            <Dashboard />
            <div className="profile-card rounded bg-white mt-5 mb-5">
                <ChangePassword show={changePass} handleClose={hideChangePass}></ChangePassword>
                <div className="d-flex flex-column align-items-center text-center p-3 pt-4 pb-4">
                    <img src={createImageFromInitials(imageSize, fullname)} className="rounded-circle" alt=""/> 
                    <span className="font-weight-bold">{fullname}</span>
                    <span className="font-weight-bold">username: @<u>{username}</u></span>
                    <span className="text-black-50">email: {email} </span><span> </span>
                    <span className="text-black-50">Member since {member_since}</span><span> </span>
                </div>
                <div className="mt-4 pb-3 text-center"><button className="btn btn-danger profile-button" type="button" onClick={showChangePass}>Change password</button></div>
            </div>
        </div>
    )
}

export default UserProfile;