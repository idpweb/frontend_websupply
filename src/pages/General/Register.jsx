import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { Form, Alert } from "react-bootstrap";
import { Button } from "react-bootstrap";
import { useUserAuth } from "../../context/UserAuthContext";

const Register = () => {
  const [email, setEmail] = useState("");
  const [error, setError] = useState("");
  const [password, setPassword] = useState("");
  const { signUp } = useUserAuth();
  let navigate = useNavigate();

  const handleSubmit = async (e) => {
    e.preventDefault();
    setError("");
    try {
      await signUp(email, password);
      navigate("/");
    } catch (err) {
      setError(err.message);
    }
  };

  return (
    <div className="Auth-form-container">
        <form className="Auth-form" onSubmit={handleSubmit}>
            <div className="Auth-form-content">
                <h3 className="Auth-form-title">Register</h3>
                {error && <Alert variant="danger">{error}</Alert>}
                
                <div className="form-group mt-3" controllid="formBasicEmail">
                    <label>Email address</label>
                    <input
                        type="email"
                        className="form-control mt-1"
                        placeholder="Enter email"
                        onChange={(e) => setEmail(e.target.value)}
                    />
                </div>

                <div className="form-group mt-3" controllid="formBasicPassword">
                    <label>Password</label>
                    <input
                        type="password"
                        className="form-control mt-1"
                        placeholder="Enter password"
                        onChange={(e) => setPassword(e.target.value)}
                    />
                </div>

                <div className="form-group mt-3" controllid="formBasicUsername">
                    <label>Username</label>
                    <input
                        type="text"
                        className="form-control mt-1"
                        placeholder="Enter username"
                        //onChange={(e) => setPassword(e.target.value)}
                    />
                </div>

                <div className="form-group mt-3" controllid="formBasicFullname">
                    <label>Full Name</label>
                    <input
                        type="text"
                        className="form-control mt-1"
                        placeholder="Enter fullname"
                        //onChange={(e) => setPassword(e.target.value)}
                    />
                </div>
                <br/>
                <div className="d-grid gap-2">
                    <Button variant="primary" type="Submit">
                        Sign up
                    </Button>
                </div>
                <hr/>
                <div className="p-4 box mt-3 text-center">
                    Already have an account? <Link to="/">Log In</Link>
                </div>
            </div>
        </form>
    </div>
    )
};

export default Register;