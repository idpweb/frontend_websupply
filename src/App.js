import Router from './utils/Routing';

function App() {
  return (
    <Router />
  );
}

export default App;
