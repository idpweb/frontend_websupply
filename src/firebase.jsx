import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyA19TE7IVUTzCZXL-_mdsZCE43bAIWcS8c",
  authDomain: "pweb-dev.firebaseapp.com",
  projectId: "pweb-dev",
  storageBucket: "pweb-dev.appspot.com",
  messagingSenderId: "961123184891",
  appId: "1:961123184891:web:5cc3599dc53a25e401805e"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export default app;