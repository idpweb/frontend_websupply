import React from 'react';
import Modal from 'react-bootstrap/Modal';
import ModalBody from 'react-bootstrap/ModalBody';
import ModalHeader from 'react-bootstrap/ModalHeader';
import ModalTitle from 'react-bootstrap/ModalTitle';
import { createImageFromInitials } from '../utils/ProfilePictureUtils';

const ProfileCard = ( {show, handleClose, fullname} ) => {
    const imageSize = 150;
    const username = "raton";
    const email = "raton@gmail.com";
    const member_since = "22/10/1997";

    /* Get user data based on username / message index */
    return(
        <Modal show={show} onHide={handleClose}>
            <ModalHeader closeButton>
                <ModalTitle>{fullname}`s profile</ModalTitle>
            </ModalHeader>
            <ModalBody>
                <div className="d-flex flex-column align-items-center text-center popup-profile-picture">
                    <img src={createImageFromInitials(imageSize, fullname)} className="rounded-circle" alt=""/> 
                    <span className="font-weight-bold">{fullname}</span>
                    <span className="font-weight-bold">username: <u>@{username}</u></span>
                    <span className="text-black-50">email: {email} </span><span> </span>
                    <span className="text-black-50">Member since {member_since}</span><span> </span>
                </div>
            </ModalBody>
        </Modal>
  )
}

export default ProfileCard;
