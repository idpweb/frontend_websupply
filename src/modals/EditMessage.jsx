import React, { useState } from 'react';
import Modal from 'react-bootstrap/Modal';
import ModalBody from 'react-bootstrap/ModalBody';
import ModalHeader from 'react-bootstrap/ModalHeader';
import ModalTitle from 'react-bootstrap/ModalTitle';
import { Button, Form } from 'react-bootstrap';

const resources = ["Food", "Water", "Clothing", "Shelter", "Medical Supplies"]
const locations = [
    "Kyiv", "Kharkiv", "Odessa", "Dnipro",
    "Donetsk", "Zaporizhzhia", "Lviv", "Kryvyi Rih",
    "Mykolaiv", "Sevastopol", "Mariupol", "Luhansk",
    "Vinnytsia", "Makiivka", "Chernobyl", "Dubrovytsia",
    "Derazhnia", "Liuboml", "Valky", "Novodnistrovsk",
    "Radyvyliv", "Sokyriany", "Verkhivtseve", "Zalishchyky",
    "Pereshchepyne", "Horodenka", "Tysmenytsia", "Tiachiv", "Semenivka",
    "Bucharest", "Iasi", "Vaslui", "Suceava"];
locations.sort(function (a, b) {
    return a.localeCompare(b);
});
const form_fields = ["type", "resource", "location", "quantity", "deadline", "description"]
const quantity_min = 0;
const quantity_max = 500;

const EditMessage = ( { show, handleClose, msgProps } ) => {
    const [validated, setValidated] = useState(false);

    const handleSubmit = (event) => {
        event.preventDefault();
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }

        /* Send to backend */
        const formData = new FormData(event.target);
        const formDataObj = Object.fromEntries(formData.entries());
        /* Prints formated json */
        console.log(formDataObj);

        setValidated(true);
        handleClose();
    };

    const [qunatityVal, setQuantityVal] = useState(msgProps.quantity);
    const decodeQuantity = (value) => {
        value = isNaN(value) ? 0 : value;
        value = Math.min(Math.max(value, quantity_min), quantity_max);
        return value;
    };

    return(
        <Modal show={show} onHide={handleClose}>
            <ModalHeader closeButton>
                <ModalTitle>Edit message #{msgProps.idNum}</ModalTitle>
            </ModalHeader>
            <ModalBody>
                <Form validated={validated} onSubmit={handleSubmit}>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Message type</Form.Label>
                        <div className='inline-radio  mr-3'>
                            <Form.Check
                                inline
                                type="radio"
                                name={form_fields[0]}
                                label="Request"
                                value="Request"
                                defaultChecked
                            />
                            <Form.Check
                                inline
                                type="radio"
                                name={form_fields[0]}
                                label="Offer"
                                value="Offer"
                            />
                        </div>

                        <Form.Label className="mt-3">Resource</Form.Label>
                        <Form.Select name={form_fields[1]} aria-label="Default select example" defaultValue={msgProps.resource}>
                            {resources.map(( item, index ) => (
                                <option key={index} value={item}>{item}</option>
                            ))}
                        </Form.Select>

                        <Form.Label className="mt-3">Location</Form.Label>
                        <Form.Select name={form_fields[2]} defaultValue={msgProps.location}>
                            {locations.map(( item, index ) => (
                                <option key={index} value={item}>{item}</option>
                            ))}
                        </Form.Select>

                        <Form.Label className="mt-3">Quantity: {qunatityVal} kg</Form.Label>
                        <Form.Range name={form_fields[3]}
                            value={qunatityVal} min={quantity_min} max={quantity_max}
                            onChange={changeEvent => setQuantityVal(changeEvent.target.value)}
                            >
                        </Form.Range>
                        <Form.Control
                            value={qunatityVal}
                            onChange={changeEvent => setQuantityVal(decodeQuantity(changeEvent.target.value))}>
                        </Form.Control>

                        <Form.Label className="mt-3"> Deadline </Form.Label>
                        <Form.Control require name={form_fields[4]} type='date' min={new Date().toISOString().slice(0, 10)}>
                        </Form.Control>

                        <Form.Label className="mt-3"> Description: </Form.Label>
                        <Form.Control required defaultValue={msgProps.description} name={form_fields[5]} as="textarea" rows="5"/>
                    </Form.Group>
                    <Button type="submit">
                        {/* Change this to handleSubmit */}
                        Submit
                    </Button>
                </Form>
            </ModalBody>
        </Modal>
    )
}

export default EditMessage;
