import React, { useState } from 'react';
import Modal from 'react-bootstrap/Modal';
import ModalBody from 'react-bootstrap/ModalBody';
import ModalHeader from 'react-bootstrap/ModalHeader';
import ModalTitle from 'react-bootstrap/ModalTitle';
import { Button, Form } from 'react-bootstrap';

const resources = ["Food", "Water", "Clothing", "Shelter", "Medical Supplies"]
const locations = ["Branceni", "Bucharest", "Vaslui", "Baicoi"]
const form_fields = ["type", "resource", "location", "quantity", "deadline", "description"]
const quantity_min = 0;
const quantity_max = 500;

const ChangePassword = ( { show, handleClose } ) => {
    const [validated, setValidated] = useState(false);
    const [ errors, setErrors ] = useState({})

    const handleSubmit = (event) => {
        event.preventDefault();
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }

        /* Send to backend */
        const formData = new FormData(event.target);
        const formDataObj = Object.fromEntries(formData.entries());
        /* Prints formated json */
        console.log(formDataObj);

        /* Check current password */
        if (formDataObj.newPass === formDataObj.confirmPass) {
            setValidated(true);
            handleClose();
        }
    };

    return(
        <Modal show={show} onHide={handleClose}>
            <ModalHeader closeButton>
                <ModalTitle>Change Password</ModalTitle>
            </ModalHeader>
            <ModalBody>
                <Form validated={validated} onSubmit={handleSubmit}>
                    <Form.Group className="mb-3">
                        <Form.Label className="mt-3"> Current Password: </Form.Label>
                        <Form.Control required name="currentPass" type="password" id="currentPass"/>

                        <Form.Label className="mt-3"> New Password: </Form.Label>
                        <Form.Control required name="newPass" type="password" id="newPass"/>

                        <Form.Label className="mt-3"> Confirm Password: </Form.Label>
                        <Form.Control required name="confirmPass" type="password" id="confirmPass"/>

                        <Form.Control.Feedback type='invalid'>
                            Coi
                        </Form.Control.Feedback>
                    </Form.Group>
                    <Button type="submit">
                        {/* Change this to handleSubmit */}
                        Submit
                    </Button>
                </Form>
            </ModalBody>
        </Modal>
    )
}

export default ChangePassword;
